# syntax=docker/dockerfile:1

ARG NIFI_REGISTRY_VERSION=1.23.2

# Agregamos Step Cli para iteractuar con Step CA
FROM smallstep/step-cli as step

# Basado en la ultima version de Nifi!
FROM apache/nifi-registry:${NIFI_REGISTRY_VERSION}

# Usamos Root
USER root

# Instalar step
COPY --from=step /usr/local/bin/step /usr/local/bin/

# Instalamos gomplate para manipular templates
ARG GOMPLATE_VERSION="3.11.5"
RUN wget https://github.com/hairyhenderson/gomplate/releases/download/v${GOMPLATE_VERSION}/gomplate_linux-amd64 -O /usr/bin/gomplate && \
    chmod +x /usr/bin/gomplate  

# Instalar dependencias del SO
RUN apt-get update -y && apt-get install -y \
    curl jq git \
    && rm -rf /var/lib/apt/lists/*

# Copiar archivos necesarios desarrollados.
COPY root/ /

# Activamos la Ejecucion del entrypoint
RUN chmod +x /opt/nifi-registry/scripts/start-siriux.sh

# Colocamos los permisos adecuados
RUN mkdir -p /opt/certs && chown -R nifi:nifi /opt/certs

# ADDING TRUST internal certificate
ONBUILD USER root
ONBUILD ARG CA_URL
ONBUILD ARG CA_FINGERPRINT
ONBUILD ENV CA_URL=${CA_URL}
ONBUILD ENV CA_FINGERPRINT=${CA_FINGERPRINT}
ONBUILD RUN <<EOT
    if [ -n "${CA_URL}" ]; then
        echo "CA_URL: ${CA_URL}"
        echo "CA_FINGERPRINT: ${CA_FINGERPRINT}"
        mkdir -p /opt/step-ca
        STEPPATH=/opt/step-ca step ca bootstrap --ca-url "${CA_URL}" --fingerprint "${CA_FINGERPRINT}" --install --force
        chown -R nifi:nifi /opt/step-ca
    fi
EOT
ONBUILD USER nifi

# De nuevo usuario nifi
USER nifi

# Web HTTP(s) ports
EXPOSE 18080 18443

# Directorio por defecto
WORKDIR ${NIFI_REGISTRY_HOME}

# Apply configuration and start NiFi
#
# We need to use the exec form to avoid running our command in a subshell and omitting signals,
# thus being unable to shut down gracefully:
# https://docs.docker.com/engine/reference/builder/#entrypoint
#
# Also we need to use relative path, because the exec form does not invoke a command shell,
# thus normal shell processing does not happen:
# https://docs.docker.com/engine/reference/builder/#exec-form-entrypoint-example
ENTRYPOINT ["../scripts/start-siriux.sh"]
