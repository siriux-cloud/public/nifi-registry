#!/bin/sh -e

if [ "${NIFI_REGISTRY_FLOW_PROVIDER}" = "git" ]; then
    # Verificamos si git esta configurado
    if [ ! -d "${NIFI_REGISTRY_HOME}/flow_storage/.git" ]; then
        git -C "${NIFI_REGISTRY_HOME}/flow_storage" init -b main
    fi

    if [ -n "${NIFI_REGISTRY_GIT_SSH_URL}" ]; then
        # Registramos la llave pública del servidor git
        echo "Registrando la llave pública del servidor git"
        host_git_server="$(echo "${NIFI_REGISTRY_GIT_SSH_URL}" | cut -d'@' -f2 | cut -d':' -f1)"
        ssh-keyscan "${host_git_server}" > "${HOME}/.ssh/known_hosts"

        # Debemos chequear previamente si ya existe el remote origin apuntando a la URL
        if ! git -C "${NIFI_REGISTRY_HOME}/flow_storage" remote -v | grep "${NIFI_REGISTRY_GIT_SSH_URL}"; then
            echo "Agregando el remote origin a la URL ${NIFI_REGISTRY_GIT_SSH_URL}"
            git -C "${NIFI_REGISTRY_HOME}/flow_storage" remote add origin "${NIFI_REGISTRY_GIT_SSH_URL}"
            git -C "${NIFI_REGISTRY_HOME}/flow_storage" branch -M main
            git -C "${NIFI_REGISTRY_HOME}/flow_storage" pull origin main --allow-unrelated-histories
            git -C "${NIFI_REGISTRY_HOME}/flow_storage" push -uf origin main
        fi
    fi


    # Si no existe el archivo de configuración de git, lo creamos
    if [ -n "${NIFI_REGISTRY_GIT_USER_EMAIL}" ]; then
        git config --file "${NIFI_REGISTRY_HOME}/flow_storage/.git/config" user.email "${NIFI_REGISTRY_GIT_USER_EMAIL}"
    fi

    if [ -n "${NIFI_REGISTRY_GIT_USER_NAME}" ]; then
        git config --file "${NIFI_REGISTRY_HOME}/flow_storage/.git/config" user.name "${NIFI_REGISTRY_GIT_USER_NAME}"
    fi
fi
