#!/bin/sh -e

set -x

# Valores por defecto tom,ados de sus variable de entorno
CA_URL="${NIFI_REGISTRY_SECURITY_STEP_CA_URL}"
CA_FINGERPRINT="${NIFI_REGISTRY_SECURITY_STEP_CA_FINGERPRINT}"
DNS_DOMAIN="${NIFI_REGISTRY_SECURITY_STEP_DNS_DOMAIN}"
STEPPATH="${NIFI_REGISTRY_SECURITY_STEPPATH:-/opt/certs}"
STEP_BOOTSTRAP_PATH="${NIFI_REGISTRY_SECURITY_STEP_BOOTSTRAP_PATH}"
STEP_CA_PASSWORD_FILE="${NIFI_REGISTRY_SECURITY_STEP_CA_PASSWORD_FILE}"
SAN_1="${NIFI_REGISTRY_SECURITY_STEP_SAN_1}"
SAN_2="${NIFI_REGISTRY_SECURITY_STEP_SAN_2}"
SAN_3="${NIFI_REGISTRY_SECURITY_STEP_SAN_3}"
CERT_EXPIRATION="${NIFI_REGISTRY_SECURITY_STEP_CA_CERT_EXPIRATION:-2160h}"
KEYSTORE_PASSWORD="${KEYSTORE_PASSWORD}"
TRUSTSTORE_PASSWORD="${TRUSTSTORE_PASSWORD}"

# Validamos los parametros
if [ -z "${CA_URL}" ]; then
    echo "La variable de entorno NIFI_REGISTRY_SECURITY_STEP_CA_URL es requerida"
    exit 1
fi

if [ -z "${CA_FINGERPRINT}" ]; then
    echo "La variable de entorno NIFI_REGISTRY_SECURITY_STEP_CA_FINGERPRINT es requerida"
    exit 1
fi

if [ -z "${DNS_DOMAIN}" ]; then
    echo "La variable de entorno NIFI_REGISTRY_SECURITY_STEP_DNS_DOMAIN es requerida"
    exit 1
fi

if [ -z "${STEPPATH}" ]; then
    echo "La variable de entorno NIFI_REGISTRY_SECURITY_STEPPATH es requerida"
    exit 1
fi

if [ -z "${STEP_BOOTSTRAP_PATH}" ]; then
    STEP_BOOTSTRAP_PATH="/opt/step-ca"
fi

if [ -z "${STEP_CA_PASSWORD_FILE}" ]; then
    echo "La variable de entorno NIFI_REGISTRY_SECURITY_STEP_CA_PASSWORD_FILE es requerida"
    exit 1
fi

if [ ! -f "${STEP_CA_PASSWORD_FILE}" ]; then
    echo "El archivo ${STEP_CA_PASSWORD_FILE} no existe"
    exit 1
fi

if [ -z "${SAN_1}" ]; then
    $SAN_1="${DNS_DOMAIN}"
fi

if [ -z "${SAN_2}" ]; then
    $SAN_2="${DNS_DOMAIN}"
fi

if [ -z "${SAN_3}" ]; then
    $SAN_3="${DNS_DOMAIN}"
fi

if [ -z "${CERT_EXPIRATION}" ]; then
    $CERT_EXPIRATION="2160h"
fi

if [ -z "${KEYSTORE_PASSWORD}" ]; then
    echo "El parametro --keystore-password es requerido"
    exit 1
fi

if [ -z "${TRUSTSTORE_PASSWORD}" ]; then
    echo "El parametro --truststore-password es requerido"
    exit 1
fi

# Creamos la carpeta de certificados
mkdir -p "${STEPPATH}/server"

# Inicializamos el step-ca dentro de este container
#if [ ! -f "${STEP_BOOTSTRAP_PATH}/certs/root_ca.crt" ]; then
# STEPPATH="${STEP_BOOTSTRAP_PATH}" step ca bootstrap --ca-url "${CA_URL}" --fingerprint "${CA_FINGERPRINT}" --install --force || true
#fi

# Generamos el certificado para el servidor de nifi-registry
STEPPATH="${STEPPATH}" step ca certificate \
    "${DNS_DOMAIN}" \
    "${STEPPATH}/server.crt" \
    "${STEPPATH}/server.key" \
    --provisioner admin \
    --provisioner-password-file "${STEP_CA_PASSWORD_FILE}" \
    --not-after "$CERT_EXPIRATION" \
    --force \
    --san "${DNS_DOMAIN}" \
    --san "${SAN_1:-${DNS_DOMAIN}}" \
    --san "${SAN_2:-${_DNS_DOMAIN}}" \
    --san "${SAN_3:-${_DNS_DOMAIN}}" \
    --ca-url "${CA_URL}" \
    --root "${STEP_BOOTSTRAP_PATH}/certs/root_ca.crt"

# Inspeccionamos el certificado
STEPPATH="${STEPPATH}" step certificate inspect --short "${STEPPATH}/server.crt"

# Unimos el certificado y la llave en un solo archivo p12
openssl pkcs12 \
    -export \
    -out "${STEPPATH}/server.p12" \
    -inkey "${STEPPATH}/server.key" \
    -in "${STEPPATH}/server.crt" \
    -name "${DNS_DOMAIN}" \
    --passout "pass:${KEYSTORE_PASSWORD}"

# Creamos el keystore
[ -f "${STEPPATH}/keystore.jks" ] && rm -f "${STEPPATH}/keystore.jks"
keytool -importkeystore \
    -noprompt \
    -srckeystore ${STEPPATH}/server.p12 \
    -srcstoretype pkcs12 \
    -srcalias "${DNS_DOMAIN}" \
    -destkeystore "${STEPPATH}/keystore.jks" \
    -deststoretype jks \
    -destalias "${DNS_DOMAIN}" \
    -deststorepass "${KEYSTORE_PASSWORD}" \
    -srcstorepass "${KEYSTORE_PASSWORD}"

# Agregamos el certificado de la CA al keystore
keytool -import \
    -noprompt \
    -file ${STEP_BOOTSTRAP_PATH}/certs/root_ca.crt \
    -keystore ${STEPPATH}/keystore.jks \
    -storepass "${KEYSTORE_PASSWORD}"

# Creamos el truststore
[ -f "${STEPPATH}/truststore.jks" ] && rm -f "${STEPPATH}/truststore.jks"
keytool -import \
    -noprompt \
    -file "${STEP_BOOTSTRAP_PATH}/certs/root_ca.crt" \
    -keystore "${STEPPATH}/truststore.jks" \
    -storepass "${TRUSTSTORE_PASSWORD}"
