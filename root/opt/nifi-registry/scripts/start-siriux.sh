#!/bin/bash -e

# Direccion del script para sh POSIX (no bash),
scripts_dir='/opt/nifi-registry/scripts'

[ -f "${scripts_dir}/common.sh" ] && . "${scripts_dir}/common.sh"

# El ultimo grupo de mapping tratado
last_mmapping_gropup=""

# Obtenemos todas las variables de entorno que tengan esta forma NIFI_REGISTRY_SECURITY_IDENTITY_MAPPING_*_*
# y debemos por cada una de esas propiedad es5tablecer un valor en el archivo nifi-registry.properties
# con el formato nifi.registry.security.identity.mapping.<name>.<dvalue>=<value> con name y dn en minusculas.
for variable in $(env | grep -E '^NIFI_REGISTRY_SECURITY_IDENTITY_MAPPING_[^_]+_.*$' | sort); do
    # Extraer el nombre y el valor de la variable
    name=$(echo "$variable" | cut -d'=' -f1)
    valor=$(echo "$variable" | cut -d'=' -f2-)
    grupo=$(echo "$name" | cut -d'_' -f6)
    
    # Formatear la línea recordar en el name debe ir el nombre en minúsculas y remplazar _ por . (punto)
    linea="nifi.registry.security.identity.mapping.$(echo "$name" | cut -d'_' -f7 | tr '[:upper:]' '[:lower:]').$(echo "$name" | cut -d'_' -f6 | tr '[:upper:]' '[:lower:]')=$(echo "$valor")"

    echo "Agregando o actualizando la propiedad $linea en el archivo $nifi_registry_props_file"
    if [ "$last_mmapping_gropup" != "$grupo" ]; then
        if [ "$last_mmapping_gropup" = "" ]; then
            echo "" >> "$nifi_registry_props_file"
        fi
        echo "" >> "$nifi_registry_props_file"
        echo "# Identity Mapping $grupo" >> "$nifi_registry_props_file"
        last_mmapping_gropup="$grupo"
    fi

    # Agregar o actualizar en el archivo de configuración
    echo "$linea" >> "$nifi_registry_props_file"
done

# Mofdificamos authorizers.xml
echo "Modificamos authorizers.xml"
[ -f "${scripts_dir}/update_authorizer.sh" ] && . "${scripts_dir}/update_authorizer.sh"

# Configurtamos git ssh
echo "Configurando git ssh"
[ -f "${scripts_dir}/config-git-ssh.sh" ] && . "${scripts_dir}/config-git-ssh.sh"

echo "Generamos los certificados"
if [ "${NIFI_REGISTRY_SECURITY_GENERATE_STEP_CA_CERTS}" = "true" ]; then
    [ -f "${scripts_dir}/create-step-ca-cert.sh" ] && . "${scripts_dir}/create-step-ca-cert.sh"
fi

# Enviamos el control a star.sh
exec "${scripts_dir}/start.sh" ${@}
