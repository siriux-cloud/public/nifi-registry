#!/bin/sh -e

providers_file=${NIFI_REGISTRY_HOME}/conf/authorizers.xml
users_default_file=${NIFI_REGISTRY_HOME}/conf/users.xml
authorizations_default_file=${NIFI_REGISTRY_HOME}/conf/authorizations.xml
property_xpath='/authorizers'

# Si el archivo de usuarios no existe, lo creamos a partir del archivo de usuarios por defecto
if [ -n "${NIFI_REGISTRY_AUTHORIZERS_USER_FILE}" ]; then
    if [ ! -f "${NIFI_REGISTRY_AUTHORIZERS_USER_FILE}" ]; then
        if [ -f "${users_default_file}" ]; then
            cp "${users_default_file}" "${NIFI_REGISTRY_AUTHORIZERS_USER_FILE}"
        fi
    fi
fi
xmlstarlet ed --pf --inplace -u "${property_xpath}/userGroupProvider/property[@name='Users File']" -v "${NIFI_REGISTRY_AUTHORIZERS_USER_FILE:-./conf/users.xml}" "${providers_file}"

# Si el archivo de autorizaciones no existe, lo creamos a partir del archivo de autorizaciones por defecto
if [ -n "${NIFI_REGISTRY_AUTHORIZERS_AUTHORIZATIONS_FILE}" ]; then
    if [ ! -f "${NIFI_REGISTRY_AUTHORIZERS_AUTHORIZATIONS_FILE}" ]; then
        if [ -f "${authorizations_default_file}" ]; then
            cp "${authorizations_default_file}" "${NIFI_REGISTRY_AUTHORIZERS_AUTHORIZATIONS_FILE}"
        fi
    fi
fi
xmlstarlet ed --pf --inplace -u "${property_xpath}/accessPolicyProvider/property[@name='Authorizations File']" -v "${NIFI_REGISTRY_AUTHORIZERS_AUTHORIZATIONS_FILE:-./conf/authorizations.xml}" "${providers_file}"
